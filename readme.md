# Project Title

AED Assignment 4

## Purpose of the assignment

The objective of this assignment is to learn how to implement and populate complex commerce and trade models.
In this exercise, we implemented the Xerox sales process object model by mapping it to 
java classes and then populating the model with products, customer, orders, and sales personnel.

The report must answer the following questions and all in relation revenue totals:
1)	Our top 3 most popular product sorted from high to low.
2)	Our 3 best customers 
3)	Our top 3 best sales people
4)	Our total revenue for the year


### Getting Started

Clone the repository on the local machine at the appropriate location.
Download the source files and unzip them into the empty directory you just created.
Add the files to your personal repository using the command line.
