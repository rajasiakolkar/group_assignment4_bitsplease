/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aayush
 */
public class AnalysisHelper {
    
    public void display(){
        
     Map<Integer, Product> products = DataStore.getInstance().getProducts();
     Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
     Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPersons();
     Map<Integer, Order> orders = DataStore.getInstance().getOrders();
     Map<Integer, Item> items = DataStore.getInstance().getItems();
     
        System.out.println("\n"+products);
        System.out.println("\n"+customers);
        System.out.println("\n"+salesPersons);
        System.out.println("\n"+orders);
        System.out.println("\n"+items);
    
    }
    
    public void MostPopularProduct(){
        //top 3 from high to low
        Map<Integer ,Integer > itemIdAndQuant = new HashMap<>();
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        for(Item item : items.values()){
            if(itemIdAndQuant.containsKey(item.getProductId())){
                int quantity = itemIdAndQuant.get(item.getProductId());
                    quantity += item.getQuantity();
                    itemIdAndQuant.put(item.getProductId(), quantity);
                
            }else{
            itemIdAndQuant.put(item.getProductId(),item.getQuantity());
            }
        }
        
        //System.out.println("itemIdandQuantity: " + itemIdAndQuant);
        
        List<Integer> sorteditemIdAndQuant = new ArrayList<Integer>(itemIdAndQuant.values());
        Collections.sort(sorteditemIdAndQuant, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get descending list
            return o2 - o1;
            }
        });
        
        //System.out.println("sorted item and quantity: " + sorteditemIdAndQuant);
        
        List<Integer> top3List = new ArrayList<>();
        int top3= 3;
        
            for(int i:sorteditemIdAndQuant){
                while(top3 > 0){
                top3List.add(i);
                top3--;
                break;
            }
        }
            
        //System.out.println("top3List: "+top3List);
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        System.out.println("\nTop 3 Most Popular Product ->");
            for(int i: top3List ){
                
                for(int id : itemIdAndQuant.keySet()){
                    if(itemIdAndQuant.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"Product Id: "+ id +" Quantity sold: " + i);
                         num++;
                         break;
                    }   
                }      
        }
     System.out.println("-------------------------------------------------------------------------------------------------------------------");
    }
    
    public void bestCustomers(){
        //top 3
        //BEST CUSTOMER TO COST
        Map<Integer,Integer> customerToCost = new HashMap<Integer,Integer>();
        Map<Integer, Customer> customers = DataStore.getInstance().getCustomers();
        for(Customer customer:customers.values()){
            
            for(Order o: customer.getOrders()){
                
                if(customerToCost.containsKey(customer.getId())){
                    int cost = customerToCost.get(customer.getId());
                    cost+= o.getItem().getSalesPrice();
                    customerToCost.put(customer.getId(), cost);
                }else{
                customerToCost.put(customer.getId(),o.getItem().getSalesPrice());
                }
            }
            
        }
        
        //System.out.println("Customet to cost: "+ customerToCost);
        
        List<Integer> sortedCustomerToCost = new ArrayList<Integer>(customerToCost.values());
        Collections.sort(sortedCustomerToCost, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get descending list
            return o2 - o1;
            }
        });
        
        //System.out.println("sortedCustomerToCost: " + sortedCustomerToCost);
        
        List<Integer> top3List = new ArrayList<>();
        int top3= 3;
        
            for(int i:sortedCustomerToCost){
                while(top3 > 0){
                top3List.add(i);
                top3--;
                break;
            }
        }
            
        //System.out.println("top3List: "+top3List);
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        System.out.println("\nTop 3 Customers based on total money spent on purchases ->");
            for(int i: top3List ){
                
                for(int id : customerToCost.keySet()){
                    if(customerToCost.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"Customer Id: "+ id +" Total cost of purchases: $" + i);
                         num++;
                         break;
                    }   
                }      
        }
            
        // TOP 3 CUSTOMERS BASED ON QUANTITY OF PRODUCT
         Map<Integer,Integer> customerToQuantity = new HashMap<Integer,Integer>();
         for(Customer customer:customers.values()){
            
            for(Order o: customer.getOrders()){
                
                if(customerToQuantity.containsKey(customer.getId())){
                    int quantity = customerToQuantity.get(customer.getId());
                    quantity+= o.getItem().getQuantity();
                    customerToQuantity.put(customer.getId(), quantity);
                }else{
                customerToQuantity.put(customer.getId(),o.getItem().getQuantity());
                }
            }
            
        }
         //System.out.println("Customer to quantity : "+ customerToQuantity);
        
        List<Integer> sortedcustomerToQuantity = new ArrayList<Integer>(customerToQuantity.values());
        Collections.sort(sortedcustomerToQuantity, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get descending list
            return o2 - o1;
            }
        });
        
        //System.out.println("sortedcustomerToQuantity: " + sortedcustomerToQuantity);
        
        top3List = new ArrayList<>();
        top3= 3;
            for(int i:sortedcustomerToQuantity){
                while(top3 > 0){
                top3List.add(i);
                top3--;
                break;
            }
        }
            
        //System.out.println("top3List: "+top3List);
        added = new ArrayList<Integer>();
        num=1;
        System.out.println("\nTop 3 Customers based on total products purchased ->");
            for(int i: top3List ){
                
                for(int id : customerToQuantity.keySet()){
                    if(customerToQuantity.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"Customer Id: "+ id +" Total products purchased: " + i);
                         num++;
                         break;
                    }   
                }      
        }
       System.out.println("-------------------------------------------------------------------------------------------------------------------"); 
    }
    
    public void bestSalesPeople(){
       //top 3 
       //BEST SALES PEOPLE ACCORDING TO TOTAL SALES PRICE
       Map<Integer,Integer> saleIdToCost = new HashMap<Integer,Integer>();
        Map<Integer, SalesPerson> salesPersons = DataStore.getInstance().getSalesPersons();
        
        for(SalesPerson salesPerson:salesPersons.values()){  
            for(Order o: salesPerson.getOrders()){
                
                if(saleIdToCost.containsKey(salesPerson.getSalesPersonId())){
                    int cost = saleIdToCost.get(salesPerson.getSalesPersonId());
                    cost+= o.getItem().getSalesPrice();
                    saleIdToCost.put(salesPerson.getSalesPersonId(), cost);
                }else{
                saleIdToCost.put(salesPerson.getSalesPersonId(),o.getItem().getSalesPrice());
                }
            }    
        }
        
        //System.out.println("saleIdToCost: "+ saleIdToCost);
        
        //System.out.println("Customet to cost: "+ customerToCost);
        
        List<Integer> sortedSaleIdToCost = new ArrayList<Integer>(saleIdToCost.values());
        Collections.sort(sortedSaleIdToCost, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get descending list
            return o2 - o1;
            }
        });
        
        //System.out.println("sortedSaleIdToCost: " + sortedSaleIdToCost);
        
        List<Integer> top3List = new ArrayList<>();
        int top3= 3;
        
            for(int i:sortedSaleIdToCost){
                while(top3 > 0){
                top3List.add(i);
                top3--;
                break;
            }
        }
            
        //System.out.println("top3List: "+top3List);
        int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        System.out.println("\nTop 3 Sales People based on total sales ->");
            for(int i: top3List ){
                
                for(int id : saleIdToCost.keySet()){
                    if(saleIdToCost.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"Seller Id: "+ id +" Total sales: $" + i);
                         num++;
                         break;
                    }   
                }      
        }
            
            
            //BEST SALES PEOPLE ACCORDING TO TOTAL PRODUCTS SOLD
       Map<Integer,Integer> saleIdToQuantity = new HashMap<Integer,Integer>();
        
        
        for(SalesPerson salesPerson:salesPersons.values()){  
            for(Order o: salesPerson.getOrders()){
                
                if(saleIdToQuantity.containsKey(salesPerson.getSalesPersonId())){
                    int quantity = saleIdToQuantity.get(salesPerson.getSalesPersonId());
                    quantity+= o.getItem().getQuantity();
                    saleIdToQuantity.put(salesPerson.getSalesPersonId(), quantity);
                }else{
                saleIdToQuantity.put(salesPerson.getSalesPersonId(),o.getItem().getQuantity());
                }
            }    
        }
        
        //System.out.println("saleIdToQuantity: "+ saleIdToQuantity);
        
        
        List<Integer> sortedSaleIdToQuantity = new ArrayList<Integer>(saleIdToQuantity.values());
        Collections.sort(sortedSaleIdToQuantity, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get descending list
            return o2 - o1;
            }
        });
        
        //System.out.println("sortedSaleIdToQuantity: " + sortedSaleIdToQuantity);
        
        top3List = new ArrayList<>();
        top3= 3;
        
            for(int i:sortedSaleIdToQuantity){
                while(top3 > 0){
                top3List.add(i);
                top3--;
                break;
            }
        }
            
        //System.out.println("top3List: "+top3List);
        num = 1;
        added = new ArrayList<Integer>();
        System.out.println("\nTop 3 Sales People based on Total Products Sold ->");
            for(int i: top3List ){
                
                for(int id : saleIdToQuantity.keySet()){
                    if(saleIdToQuantity.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"Seller Id: "+ id +" Total products sold: " + i);
                         num++;
                         break;
                    }   
                }      
        }
            
      salesPersonTargetPrice();
      
      System.out.println("-------------------------------------------------------------------------------------------------------------------");   
    }
    
    public void totalRevenue(){
      // per year  
     /*
        LOGIC
       first hashmap--> KEY = productid ; VALUE = quantity*salesprice  --> summation 1
       second hashmap --> KEY = productid ; VALUE = quantity*minprice  --> summation 2
        summation 1 - summation 2
     */   
       // Hashmap for productIDQuantity
//       Map<Integer ,Integer > productIdAndQuant = new HashMap<>();
//        Map<Integer, Item> items = DataStore.getInstance().getItems();
//        for(Item item : items.values()){
//            if(productIdAndQuant.containsKey(item.getProductId())){
//                int quantity = productIdAndQuant.get(item.getProductId());
//                    quantity += item.getQuantity();
//                    productIdAndQuant.put(item.getProductId(), quantity);
//                
//            }else{
//            productIdAndQuant.put(item.getProductId(),item.getQuantity());
//            }
//        }
//        
//        System.out.println("productIdAndQuant: " + productIdAndQuant); 
//        
//        //HashMap for productIDSales
//        Map<Integer ,Integer > productIDSales = new HashMap<>();
//        for(Item item : items.values()){
//            if(productIDSales.containsKey(item.getProductId())){
//                int cost = productIDSales.get(item.getProductId());
//                    cost += item.getSalesPrice();
//                    productIDSales.put(item.getProductId(), cost);
//                
//            }else{
//            productIDSales.put(item.getProductId(),item.getSalesPrice());
//            }
//        }
//        System.out.println("\nproductIDSales: " + productIDSales); 
//        
//        Map<Integer, Integer> productIdMultiplication = new HashMap<>();
//        
//        for(int i: productIdAndQuant.keySet()){
//            for(int j: productIDSales.keySet()){
//                if(i==j)
//                productIdMultiplication.put(i,productIdAndQuant.get(i)*productIDSales.get(j));
//            }
//        }
//        System.out.println("\nproductIdMultiplication: " + productIdMultiplication);
//        
//        int summation1=0;
//        
//        for(int i:productIdMultiplication.values()){
//            summation1 +=i;
//        }
//        
//        System.out.println("\nSummation1: " + summation1);
//        
//        System.out.println("\nTotal Revenue--> "+ summation1);
//        
//        // Hashmap for productIDMinprice in Product
//        Map<Integer ,Double > productIDMinprice = new HashMap<>();
//        Map<Integer, Product> products = DataStore.getInstance().getProducts();
//        for(Product p:products.values()){
//            productIDMinprice.put(p.getProductId(), p.getMinPrice());
//        }
//        
//        System.out.println("\nproductIDMinprice: "+ productIDMinprice);
//        
//        
//        // Hashmap for productIDMinprice in sales
//        Map<Integer ,Double > productIDMinPrice = new HashMap<>();
//        for(Item item : items.values()){
//            
//            if(productIDMinPrice.containsKey(item.getProductId())){
//                double cost = productIDMinPrice.get(item.getProductId());
//                    cost += productIDMinprice.get(item.getProductId());
//                    productIDMinPrice.put(item.getProductId(), cost);
//                
//            }else{
//            productIDMinPrice.put(item.getProductId(),productIDMinprice.get(item.getProductId()));
//            }
//            
//        }
//        System.out.println("\nproductIDMinPrice: " + productIDMinPrice);
//        
//        Map<Integer,Double> minMul = new HashMap<>();
//        
//        for(int i: productIdAndQuant.keySet()){
//            for(int d : productIDMinPrice.keySet()){
//                if(i==d){
//                    minMul.put(i,productIdAndQuant.get(i)*productIDMinPrice.get(d));
//                }
//            }
//        }
//        
//        System.out.println("\nminMul: " + minMul);
//        
//        double summation2 = 0;
//        
//        for(double d:minMul.values()){
//            summation2 +=d; 
//        }
//        
//        System.out.println("\nsummation2: "+ summation2);
//        
//        System.out.println("\nTotal Profit--> " + (summation1-summation2));
//        
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        double counter =0;
        double minimumPrice = 0;
        double newCount = 0;
        
        for(Item item : items.values()){
            int productId = item.getProductId();
            counter += (item.getQuantity()*item.getSalesPrice());
            
            if(products.containsKey(item.getProductId())){
                minimumPrice = products.get(item.getProductId()).getMinPrice();
                newCount += item.getQuantity()*minimumPrice;
            }
        }
        System.out.println("");
        System.out.println("TOTAL REVENUE:               "+ counter);
        System.out.println("Minimum Revenue for profit:  "+ newCount);
        System.out.println("------------------------------------------");
        System.out.println("TOTAL PROFIT:                "+ (counter-newCount));
        System.out.println("");
        
    System.out.println("-------------------------------------------------------------------------------------------------------------------");       
    }
    
    public void salesPersonTargetPrice(){
        
        Map<Integer, Item> items = DataStore.getInstance().getItems();
        Map<Integer, Product> products = DataStore.getInstance().getProducts();
        Map<Integer, Order> orders = DataStore.getInstance().getOrders();
        Map<Integer, Integer> hmap = new HashMap<Integer, Integer>();//key -> sales id // value->products sold above target price
        double targetPrice =0;
        int productCount = 0;
        
        for(Item item : items.values()){
            for(Product product: products.values()){
                for(Order order:orders.values()){
                    
                    //target price
                    if(item.getProductId()== product.getProductId() && item.getProductId()== order.getItem().getProductId() 
                            && item.getItemId()==order.getItem().getItemId()){
                        
                        targetPrice = product.getTargetPrice();
                        
                        if(item.getSalesPrice() >= targetPrice){
//                            System.out.println("getSalesPrice() >= targetPrice");
//                            System.out.println("item -> "+ item);
//                            System.out.println("product-> "+ product);
//                            System.out.println("order-> "+ order);
                            
                            productCount = 0;
                            if(hmap.containsKey(order.getSalesId())){
                                productCount=hmap.get(order.getSalesId());
                                productCount++;
                                hmap.put(order.getSalesId(),productCount);
                            }else{
                            productCount++;
                            hmap.put(order.getSalesId(),productCount);
                            }
//                            System.out.println("salesId: " + order.getSalesId() +" products: "+ productCount);
//                            System.out.println("");
                        }
                    }
                    
                    
               }
            }
        }
        
        //System.out.println(hmap);
        
        List<Integer> sortedSalesIdProducts = new ArrayList<Integer>(hmap.values());
        Collections.sort(sortedSalesIdProducts, new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2) {
            //so as to get descending list
            return o2 - o1;
            }
        });
        
       List<Integer>top3List = new ArrayList<>();
       int top3= 3;
        
            for(int i:sortedSalesIdProducts){
                while(top3 > 0){
                top3List.add(i);
                top3--;
                break;
            }
        } 
       
       int num = 1;
        List<Integer> added = new ArrayList<Integer>();
        System.out.println("\nTop 3 Sales People based on Products sold above target price ->");
            for(int i: top3List ){
                
                for(int id : hmap.keySet()){
                    if(hmap.get(id) == i && !added.contains(id)){
                         added.add(id);
                         System.out.println(num + ") "+"Seller Id: "+ id +" Products sold above target price: " + i);
                         num++;
                         break;
                    }   
                }      
        }
    }
}
