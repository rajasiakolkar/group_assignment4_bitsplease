/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class SalesPerson {
    int salesPersonId;
    int productId;
    List<Order> orders;
    //List orders

    public SalesPerson(int salesPersonId, int productId) {
        this.salesPersonId = salesPersonId;
        this.productId=productId;
        orders = new ArrayList<>();
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    
    public int getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(int salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }


    @Override
    public String toString() {
        return "\nSalesPerson{" + "salesPersonId=" + salesPersonId + ", productId=" + productId + ", orders=" + orders + '}';
    }
    
    
}

