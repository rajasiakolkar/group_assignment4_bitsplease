/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analytics.AnalysisHelper;
import assignment_4.analytics.DataStore;
import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {

    DataGenerator generator;
    AnalysisHelper helper;
    
    public GateWay() throws IOException{
        generator = DataGenerator.getInstance();
        helper =  new AnalysisHelper();
    }
    
    
    public static void main(String[] args) throws IOException {
        GateWay gate = new GateWay();
        gate.readData();
        
    }
    
    public void readData() throws IOException{
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        //printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
            Item item = generateItem(orderRow);
            Order order = generateOrder(orderRow, item);
            generateCustomer(orderRow,order);
            generateSalesPerson(orderRow,order);
        }
        
        
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        //printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            generateProduct(prodRow);
        }
        
        runAnalysis();
    }
    
    public void generateProduct(String[] row){
        int productId = Integer.parseInt(row[0]);
        double minPrice = Double.parseDouble(row[1]);
        double maxPrice = Double.parseDouble(row[2]);
        double targetPrice = Double.parseDouble(row[3]);
        
        Product p = new Product(productId,minPrice,maxPrice,targetPrice);
        DataStore.getInstance().getProducts().put(productId, p);
    }
    
    
    public Item generateItem(String[] row){
        int productId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        int itemId = Integer.parseInt(row[1]);
        
        Item i = new Item(itemId,productId,salesPrice,quantity);
        DataStore.getInstance().getItems().put(itemId, i);
        return i;
    
    }
    
    public Order generateOrder(String[] row ,Item item){
        int orderId = Integer.parseInt(row[0]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        
//        int productId = Integer.parseInt(row[2]);
//        int salesPrice = Integer.parseInt(row[6]);
//        int quantity = Integer.parseInt(row[3]);        
//        Item item = new Item(productId,salesPrice,quantity);
        
        Order o = new Order(orderId,salesId,customerId,item);
        DataStore.getInstance().getOrders().put(orderId, o);
        
        return o;
    
    }
    
    public void generateCustomer(String[] row , Order order){
        int id = Integer.parseInt(row[5]);
        Map<Integer,Customer> customer = DataStore.getInstance().getCustomers();
        
        if(customer.containsKey(id)){
            customer.get(id).getOrders().add(order);
        }else{
        Customer c=new Customer(id);
        c.getOrders().add(order);
        customer.put(id, c);
        }

    }
    
    public void generateSalesPerson(String[] row , Order o){
         int salesPersonId = Integer.parseInt(row[4]);
         int productId = Integer.parseInt(row[2]);
       
         Map<Integer, SalesPerson> salesPerson = DataStore.getInstance().getSalesPersons();
         
         if(salesPerson.containsKey(salesPersonId)){
             salesPerson.get(salesPersonId).getOrders().add(o);
         }else{
             SalesPerson sp = new SalesPerson(salesPersonId,productId);
             sp.getOrders().add(o);
             salesPerson.put(salesPersonId,sp);
         }
            
   
    }
    
    public void runAnalysis(){
        //helper.display();
        helper.MostPopularProduct();
        helper.bestCustomers();
        helper.bestSalesPeople();
        helper.totalRevenue();
        
       // helper.salesPersonTargetPrice();
    }
    
}
